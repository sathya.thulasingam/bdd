package com.eurail.bdd.pageobj;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.eurail.bdd.DriverFactory;

public abstract class AbstractPage {
	
	static final Logger logger = LogManager.getLogger(AbstractPage.class.getName());
	WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(), 60);
	Properties prop = new Properties();
	
	public enum WaitType {
		ELEMENT_CLICKABLE, ELEMENT_PRESENCE, ELEMENT_VISIBLE
	}

	public enum DropDownSltType {
		TEXT, VALUE, INDEX
	}
	

	
  public void navigate(final String value) {
    DriverFactory.getInstance().getDriver().navigate().to(value);
  }

  protected String pageTitle() {
    return DriverFactory.getInstance().getDriver().getTitle();
  }

  protected void acceptAlert() {
    (new FluentWait<>(DriverFactory.getInstance().getDriver())).withTimeout(10, TimeUnit.SECONDS)
        .pollingEvery(10, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class)
        .until(ExpectedConditions.alertIsPresent());
    DriverFactory.getInstance().getDriver().switchTo().alert().accept();
  }

  protected boolean isThere(final String name) {
    final List<WebElement> listTitles = DriverFactory.getInstance().getDriver()
        .findElements(By.xpath("//h2[contains(text(), ' " + name + " ')]"));
    return listTitles.size() == 1;
  }

  protected void editText(final String id, final String value) {
    final WebElement element = (new FluentWait<>(DriverFactory.getInstance().getDriver()))
        .withTimeout(10, TimeUnit.SECONDS).pollingEvery(10, TimeUnit.MILLISECONDS)
        .ignoring(NoSuchElementException.class)
        .until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    element.sendKeys(value);
  }

  protected void clickId(final String id) {
    final WebElement button = DriverFactory.getInstance().getDriver().findElement(By.id(id));
    final JavascriptExecutor executor = (JavascriptExecutor) DriverFactory.getInstance()
        .getDriver();
    executor.executeScript("arguments[0].click();", button);
  }

  protected void clickXpathJs(final String value) {
    final WebElement button = DriverFactory.getInstance().getDriver()
        .findElement(By.xpath("//a[contains(text(), '" + value + "')]"));
    final JavascriptExecutor executor = (JavascriptExecutor) DriverFactory.getInstance()
        .getDriver();
    executor.executeScript("arguments[0].click();", button);
  }

  protected void clickXpath(final String value) {
    DriverFactory.getInstance().getDriver()
        .findElement(By.xpath("//*[contains(text(), '" + value + "')]")).click();
  }
  
  protected void clickLinkXpath(final String value) {
	  DriverFactory.getInstance().getDriver().findElement(By.xpath("//a[@href='"+value+"']")).click();
  }
  protected boolean hasErrors() {
    final List<WebElement> errors = DriverFactory.getInstance().getDriver()
        .findElements(By.className("error"));
    return (errors.size() > 0) && errors.get(0).isDisplayed();
  }

  public void quit() {
    DriverFactory.getInstance().getDriver().quit();
  }

  public void waitForElement(By locator, WaitType wt) {

		switch (wt) {
		case ELEMENT_CLICKABLE:
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			logger.debug("Waited for element " + locator + " to be clickable.");
			break;
		case ELEMENT_PRESENCE:
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			logger.debug("Waited for presence of element " + locator);
			break;
		case ELEMENT_VISIBLE:
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			logger.debug("Waited for visibility of element " + locator);
			break;
		default:
			break;

		}

	}

	public void clickElement(By locator) {
		waitForElement(locator, WaitType.ELEMENT_CLICKABLE);
		WebElement ele = DriverFactory.getInstance().getDriver().findElement(locator);
		ele.click();
		// logger.debug("Clicked on the element "+locator);
	}

	public void enterText(By locator, String value) {
		DriverFactory.getInstance().getDriver().findElement(locator).sendKeys(value);
		logger.debug("Entered text" + value + " in field " + locator);
	}

	public void enterText(By locator, Keys value) {
		DriverFactory.getInstance().getDriver().findElement(locator).sendKeys(value);
		logger.debug("Pressed key " + value + " in field " + locator);
	}

	public void selectFromDropDown(By locator, DropDownSltType type, Object value) {
		Select slt = new Select(DriverFactory.getInstance().getDriver().findElement(locator));
		switch (type) {
		case TEXT:
			slt.selectByVisibleText((String) value);
			break;
		case VALUE:
			slt.selectByValue((String) value);
			break;
		case INDEX:
			slt.selectByIndex(Integer.valueOf((String)value));
			break;
		}

		logger.debug("Selected " + value + " in dropdown " + locator);
	}

	public String getPropertyValue(String key) throws IOException {

		String value = null;

		try {
			FileReader reader = new FileReader("./src/test/resources/loginCreds.properties");
			Properties prop = new Properties();
			prop.load(reader);
			value = prop.getProperty(key);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return value;

	}
}
