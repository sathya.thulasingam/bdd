package com.eurail.bdd.pageobj;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.eurail.bdd.DriverFactory;

@Component
@ConfigurationProperties(prefix = "loginpage")
public class LoginPage extends AbstractPage {
	
	@Value("${loginpage.emailId}")
	private String emailId;
	@Value("${loginpage.pwd}")
	private String pwd;

	public void loginWithEmailAccount() {
		waitForElement(Locators.LOGIN_EMAIL_TXTBOX, WaitType.ELEMENT_PRESENCE);
		DriverFactory.getInstance().getDriver().findElement(Locators.LOGIN_EMAIL_TXTBOX)
				.sendKeys(emailId);
		DriverFactory.getInstance().getDriver().findElement(Locators.LOGIN_PWD_TXTBOX)
				.sendKeys(pwd);

		clickElement(Locators.LOGIN_ACCOUNT_BTN);
		logger.debug("User logged in using the given credentials.");
	}

	public void loginWithFacebook() {

	}

	public void loginWithGoogle() {

	}
}
