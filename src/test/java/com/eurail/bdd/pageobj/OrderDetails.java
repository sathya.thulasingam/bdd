package com.eurail.bdd.pageobj;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.eurail.bdd.DriverFactory;

@Component
@ConfigurationProperties(prefix = "orderdetailspage")
public class OrderDetails extends AbstractPage {

	@Value("${orderdetailspage.fName}")
	private String fName;
	@Value("${orderdetailspage.lName}")
	private String lName;

	@Value("${orderdetailspage.billingCountry}")
	private String billingCountry;

	@Value("${orderdetailspage.billingCity}")
	private String billingCity;

	@Value("${orderdetailspage.postCode}")
	private String postCode;

	@Value("${orderdetailspage.street}")
	private String street;

	@Value("${orderdetailspage.phoneNo}")
	private String phoneNo;


	public void fillBillingAddress(String passType) {
		waitForElement(Locators.ORDER_MOBILE_PASS, WaitType.ELEMENT_PRESENCE);
		if (passType.equalsIgnoreCase("mobile")) {
			clickElement(Locators.ORDER_MOBILE_PASS);
		} else if (passType.equalsIgnoreCase("mobile")) {
			clickElement(Locators.ORDER_MOBILE_PASS);
		}
		// Filling billing details and submitting it
		waitForElement(Locators.BILLING_FNAME_TXTBOX, WaitType.ELEMENT_PRESENCE);
		enterText(Locators.BILLING_FNAME_TXTBOX, fName);
		enterText(Locators.BILLING_LNAME_TXTBOX, lName);
		selectFromDropDown(Locators.BILLING_COUNTRY_DRPDOWN, DropDownSltType.TEXT, billingCountry);
		enterText(Locators.BILLING_CITY_TXTBOX, billingCity);
		enterText(Locators.BILLING_POSTALCODE_TXTBOX, postCode);
		enterText(Locators.BILLING_STREET_TXTBOX, street);
		enterText(Locators.BILLING_PHONE_TXTBOX, phoneNo);
		clickElement(Locators.BILLING_ORDER_DETAILS_BTN);
	}

}
