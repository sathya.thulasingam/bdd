package com.eurail.bdd.pageobj;

import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.eurail.bdd.DriverFactory;

@Component
@ConfigurationProperties(prefix = "eurailpassespage")
public class EurailPasses extends AbstractPage {
	
	public enum PassengerCategory {
		ADULT, YOUTH, CHILD, SENIOR
	}

	public enum ClassType {
		FIRST, SECOND
	}
	
	@Value("${eurailpassespage.lstOption}")
	private String lstOption;
	
	public void selectPasses(final String passType) {
		// Navigation through menus to open global/country pass
		By passLocator = null;
		clickElement(Locators.INTERRAIL_PASSES_LNK);
		if ("global".equalsIgnoreCase(passType)) {
			passLocator = Locators.GLOBAL_PASS_LNK;
		} else {
			waitForElement(Locators.ONE_COUNTRY_PASS_LNK, WaitType.ELEMENT_CLICKABLE);
			clickElement(Locators.ONE_COUNTRY_PASS_LNK);
			passLocator = By.xpath(
					"//a[contains(@href,\"content/eurail/en/eurail-passes/one-country-pass/" + passType + "\")]");
		}
		waitForElement(passLocator, WaitType.ELEMENT_CLICKABLE);
		clickElement(passLocator);
	}

	public void addPassengers(final int adultNo, int youthNo, int childNo, int seniorNo) {
		passengerSetter(adultNo, PassengerCategory.ADULT );
		passengerSetter(youthNo,PassengerCategory.YOUTH);
		passengerSetter(childNo,PassengerCategory.CHILD);
		passengerSetter(seniorNo, PassengerCategory.SENIOR);
	}
	
	public void selectClass(String classType) {
		By classTypeLoc = null;
		if(classType.equalsIgnoreCase(ClassType.FIRST.toString())) {
			classTypeLoc = Locators.FIRST_CLASS_LNK;
		}else if(classType.equalsIgnoreCase(ClassType.SECOND.toString())) {
			classTypeLoc = Locators.SECOND_CLASS_LNK;
		}else {
			logger.warn("Invalid class passed in step definition, so proceeding with default option");
			classTypeLoc = Locators.FIRST_CLASS_LNK;
		}
		waitForElement(classTypeLoc, WaitType.ELEMENT_CLICKABLE);
		clickElement(classTypeLoc);
	}
	
	public void selectOption(String optionType) {
		By optionAddtoCartLoc = null;
		if(lstOption.contains(optionType.replaceAll(" ", "-"))) {
			optionAddtoCartLoc = By.xpath("//div[@data-cy=\"pt-pass-title-"+optionType.replaceAll(" ", "-")+"\"]//parent::div/following-sibling::div[@class=\"part part-b\"]/div/button[contains(text(),\"Add to cart\")]");
			clickElement(optionAddtoCartLoc);
		}else {
			System.out.println("pass option is invalid");
		}
		
	}
	
	public void gotoCartReview() {
		waitForElement(Locators.CHECKOUT_BASKET_LNK, WaitType.ELEMENT_CLICKABLE);
		clickElement(Locators.CHECKOUT_BASKET_LNK);
	}
	
	
	private void passengerSetter(int passengerNo, PassengerCategory passengerCategory) {
		By quantityTxt = null;
		By plusBtn = null; 
		By minusBtn = null;
		switch(passengerCategory) {
			case ADULT:
				quantityTxt=Locators.ADULT_QUANTITY_TEXT;
				plusBtn= Locators.CATEGORY_ADULT_PLUS_LINK;
				minusBtn=Locators.CATEGORY_ADULT_MINUS_LINK;
				break;
			case YOUTH:
				quantityTxt=Locators.YOUTH_QUANTITY_TEXT;
				plusBtn= Locators.CATEGORY_YOUTH_PLUS_LINK;
				minusBtn=Locators.CATEGORY_YOUTH_MINUS_LINK;
				break;
			case CHILD:
				quantityTxt=Locators.CHILD_QUANTITY_TEXT;
				plusBtn= Locators.CATEGORY_CHILD_PLUS_LINK;
				minusBtn=Locators.CATEGORY_CHILD_MINUS_LINK;
				break;
			case SENIOR:
				quantityTxt=Locators.SENIOR_QUANTITY_TEXT;
				plusBtn= Locators.CATEGORY_SENIOR_PLUS_LINK;
				minusBtn=Locators.CATEGORY_SENIOR_MINUS_LINK;
				break;
		}
		if(passengerNo!=Integer.valueOf(DriverFactory.getInstance().getDriver().findElement(quantityTxt).getAttribute("value"))) {
			while(passengerNo>Integer.valueOf(DriverFactory.getInstance().getDriver().findElement(quantityTxt).getAttribute("value"))) {
				clickElement(plusBtn);
			}
			while(passengerNo<Integer.valueOf(DriverFactory.getInstance().getDriver().findElement(quantityTxt).getAttribute("value"))) {
				clickElement(minusBtn);
			}
		}
	}
}
