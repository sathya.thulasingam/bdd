package com.eurail.bdd.pageobj;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.eurail.bdd.pageobj.AbstractPage.WaitType;

@Component
@ConfigurationProperties(prefix = "checkoutbasketreview")
public class CheckoutBasketReview extends AbstractPage {

	public void proceedToCheckout() {
		waitForElement(Locators.BASKET_REVIEW_CONTINUE_BTN, WaitType.ELEMENT_CLICKABLE);
		clickElement(Locators.BASKET_REVIEW_CONTINUE_BTN);
	}

}
