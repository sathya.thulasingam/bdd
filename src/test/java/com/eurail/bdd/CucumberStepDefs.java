package com.eurail.bdd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.eurail.bdd.pageobj.AbstractPage;
import com.eurail.bdd.pageobj.LoginPage;
import com.eurail.bdd.pageobj.OrderDetails;
import com.eurail.bdd.pageobj.CheckoutBasketReview;
import com.eurail.bdd.pageobj.EurailPasses;
import com.eurail.bdd.pageobj.LandingPage;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestApplication.class)
@SpringBootTest
public class CucumberStepDefs extends AbstractPage {

	@Autowired
	private LandingPage landingPage;
	@Autowired
	private EurailPasses eurailPassesPage;
	@Autowired
	private LoginPage loginPage;
	@Autowired
	private CheckoutBasketReview checkoutBasketReview;
	@Autowired
	private OrderDetails orderDetailsPage;

	static final Logger logger = LogManager.getLogger(CucumberStepDefs.class.getName());

	@Given("^user opens 'ie' browser and launch 'https://acc\\.eurail\\.com/en' url$")
	public void user_opens_ie_browser_and_launch_https_acc_eurail_com_en_url() throws Throwable {
		landingPage.toPage().clickOkToCookies();
		// loginPage.doLogin(user, password);
	}

	@Given("^select \"([^\"]*)\" class \"([^\"]*)\" pass for (\\d+) adult, (\\d+) youth, (\\d+) child, (\\d+) senior$")
	public void buy_class_global_pass_for_adult_youth_child_senior(String classType, String passType, int adultNo, int youthNo, int childNo, int seniorNo)
			throws Throwable {
		eurailPassesPage.selectPasses(passType);
		eurailPassesPage.addPassengers(adultNo, youthNo, childNo, seniorNo);
		eurailPassesPage.selectClass(classType);


		/* 
		 * // Fill travelers details waitForElement(Locators.TRAVELLER_DETAILS_GENDER_M,
		 * WaitType.ELEMENT_PRESENCE); enterText(Locators.TRAVELLER_DETAILS_GENDER_M,
		 * Keys.SPACE); enterText(Locators.TRAVELLER_DETAILS_FNAME_TXTBOX, "Jack");
		 * enterText(Locators.TRAVELLER_DETAILS_LNAME_TXTBOX, "Robins");
		 */	}
	
	@Given("^add \"([^\"]*)\" to cart and go to checkout$")
	public void add_to_cart_and_go_to_checkout(String option) throws Throwable {
		
		eurailPassesPage.selectOption(option);
		logger.debug("Clicked Add to cart button.");
		eurailPassesPage.gotoCartReview();
	}

	@Given("^review the cart and proceed to checkout$")
	public void review_the_cart_and_proceed_to_checkout() throws Throwable {
		checkoutBasketReview.proceedToCheckout();
	}

	@Given("^login to eurail using \"([^\"]*)\"$")
	public void login_to_eurail_using(String arg1) throws Throwable {
		loginPage.loginWithEmailAccount();
	}

	@Given("^select \"([^\"]*)\" pass and complete billing address in Order details page$")
	public void select_pass_and_complete_billing_address(String arg1) throws Throwable {
		orderDetailsPage.fillBillingAddress(arg1);
	}

	@Given("^Fill travellers details and proceed to checkout$")
	public void fill_travellers_details_and_proceed_to_checkout() throws Throwable {

	}

	@Given("^review the order and proceed to payment$")
	public void review_the_order_and_proceed_to_payment() throws Throwable {

	}
	
	@Given("^the user goes to \"([^\"]*)\" page$")
	public void the_user_goes_to_page(String arg1) throws Throwable {
		System.out.println("#######" + arg1);
	}

	@Given("^searches train journey between \"([^\"]*)\" and \"([^\"]*)\" on date \"([^\"]*)\" time \"([^\"]*)\"$")
	public void searches_train_journey_between_and_on_date_time(String arg1, String arg2, String arg3, String arg4)
			throws Throwable {
		System.out.println("#######" + arg1 + arg2 + arg3 + arg4);
	}

	@After
	public void cleanupWindows() throws Throwable {
		eurailPassesPage.quit();
	}
}
