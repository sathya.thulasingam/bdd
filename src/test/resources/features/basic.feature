Feature: Automation of Eurail Global pass flow

  Scenario: 
    Given user opens 'ie' browser and launch 'https://acc.eurail.com/en' url
    And select "second" class "global" pass for 3 adult, 0 youth, 1 child, 1 senior
    And add "10 days within 2 months" to cart and go to checkout
    And review the cart and proceed to checkout
    And login to eurail using "eurailaccount/facebook/google"
    And select "mobile" pass and complete billing address in Order details page
    And Fill travellers details and proceed to checkout
    And review the order and proceed to payment
    And the user goes to "book reservations" page
    And searches train journey between "Paris" and "Amsterdam Centraal" on date "31-12-2021" time "05:00 AM"
